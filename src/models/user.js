const {Schema, model} = require('mongoose');

const userSchema = new Schema({
    user:{
        type: String
    },
    password:{
        type: String
    }
}, {
    collection: 'users'
});


module.exports = model('User', userSchema);