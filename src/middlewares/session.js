const session = require('express-session');
const MongoStore = require('connect-mongo');

module.exports = session({
    secret: "-ESTOESUNAAllavePrivada53853-*!-",
    resave: false,
    saveUninitialized: true,
    store: MongoStore.create({mongoUrl: 'mongodb://localhost:27017/sesiones_53853'}),
    name: 'MINTIC_SESSION',
    cookie: {
        httpOnly: true,
        maxAge: 60000
    }
});