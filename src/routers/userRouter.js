const {Router} = require('express');
const UserController = require('../controllers/userController');

class UserRouter{

    constructor(){
        this.router = Router();
        this.#config();
    }

    #config(){
        //Crear un objeto tipo controlador
         const objUser = new UserController();
         //Rutas
         this.router.post('/user', objUser.register);
         this.router.post('/auth', objUser.login);
         this.router.get('/auth', objUser.verify);
    }

}

module.exports = UserRouter;